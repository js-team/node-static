# Installation
> `npm install --save @types/node-static`

# Summary
This package contains type definitions for node-static (https://github.com/cloudhead/node-static).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/node-static.

### Additional Details
 * Last updated: Thu, 08 Jul 2021 18:51:27 GMT
 * Dependencies: [@types/mime](https://npmjs.com/package/@types/mime), [@types/node](https://npmjs.com/package/@types/node)
 * Global values: none

# Credits
These definitions were written by [Ben Davies](https://github.com/Morfent).
